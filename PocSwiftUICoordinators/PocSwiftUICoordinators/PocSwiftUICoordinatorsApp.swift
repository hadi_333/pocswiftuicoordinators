//
//  PocSwiftUICoordinatorsApp.swift
//  PocSwiftUICoordinators
//
//  Created by Hadi KUDSI on 04/07/2023.
//

import SwiftUI

@main
struct PocSwiftUICoordinatorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
